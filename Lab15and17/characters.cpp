

#include <iostream>
#include <string>
#include <fstream>
#include<sstream>


#include "characters.h"



character::character (string n,string p,int l, int ex,int mex,int h, int s, int d,int e,int i ,int c,string ci, int cib) {
    name = n;
    profession=p;
    level=l;
    experience=ex;
    maxeperiencelvl=mex;
    health=h;
    statistics[0] = s;
    statistics[1] = d;
    statistics[2] = e;
    statistics[3] = i;
    statistics[4] = c;
    charitem=ci;
    charitembonus=cib;
}

character::~character(){
}

void character::add(){
    cout<<endl;
    cout<<"Enter the name of your character:"<<endl;
    cin>>name;
    int i;
    for(i=0;i<5;i++){
        cout<<"Enter value of "<<statname[i]<<" (0-10): "<<endl;
        cin>>statistics[i];
        if (statistics[i]>10 || statistics[i]<0 ){
            cout<<"Invalid value!"<<endl;
            cout<<"Enter value of "<<statname[i]<<" (0-10): "<<endl;
            cin>>statistics[i];
        }

        else{

        }
    }
    level=1;
    experience=0;
    maxeperiencelvl=100;
    health=100;
    berserker b;
    thief t;
    warrior w;
    mage m;
    bard ba;
    character ch(name,profession,level, experience,maxeperiencelvl,health,statistics[0], statistics[1],statistics[2],statistics[3] ,statistics[4]);
    choose(ch,b,t,w,m, ba);
    ch.save();
}
void character::save(){
    cout<<endl;
    cout<<"Do you want to save your character?"<<endl;
    cout<<"1.Save"<<endl;
    cout<<"2.Delete"<<endl;
    int saving;
    cin>>saving;
    if (saving==1){
        cout<<"Saving your character"<<endl;
        cout<<endl;
        string filename = name + ".txt";
        ofstream e(filename.c_str());
        e<<name<<endl;
        e<<profession<<endl;
        e<<level<<endl;
        e<<experience<<endl;
        e<<maxeperiencelvl<<endl;
        e<<health<<endl;
        e<<statistics[0]<<endl;
        e<<statistics[1]<<endl;
        e<<statistics[2]<<endl;
        e<<statistics[3]<<endl;
        e<<statistics[4]<<endl;
        e<<charitem<<endl;
        e<<charitembonus<<endl;
        e.close();
    }
    else if (saving==2) {
        cout<<"Deleting your character"<<endl;
        cout<<endl;
    }
    else {
        cout<<"Invalid value!"<<endl;
    }
}
void character::load(){
    cout<<endl;
    cout<<"Which character do you want to load?"<<endl;
    string nameofchar;
    cin>>nameofchar;
    cout<<"Loading..."<<endl;
    cout<<endl;

    string filename = nameofchar + ".txt";
    fstream file;
    nameofchar.clear();
    file.open(filename.c_str(), ios::in | ios::out );
    if(file.good()==true){
        string exch;
        file>>name;
        file>>profession;

        file>>exch;
        level=atoi(exch.c_str());
        exch.clear();

        file>>exch;
        experience=atoi(exch.c_str());
        exch.clear();

        file>>exch;
        maxeperiencelvl=atoi(exch.c_str());
        exch.clear();

        file>>exch;
        health=atoi(exch.c_str());
        exch.clear();

        file>>exch;
        statistics[0]=atoi(exch.c_str());
        exch.clear();

        file>>exch;
        statistics[1]=atoi(exch.c_str());
        exch.clear();

        file>>exch;
        statistics[2]=atoi(exch.c_str());
        exch.clear();

        file>>exch;
        statistics[3]=atoi(exch.c_str());
        exch.clear();

        file>>exch;
        statistics[4]=atoi(exch.c_str());
        exch.clear();

        file>>charitem;

        file>>exch;
        charitembonus=atoi(exch.c_str());
        exch.clear();

    }
    else{
        cout<<"Invalid value!"<<endl;
        cout<<endl;
    }
    file.close();
    character ch(name,profession,level,experience,maxeperiencelvl,health, statistics[0], statistics[1],statistics[2],statistics[3] ,statistics[4],charitem,charitembonus);
    ch.display();
}

void character::choose(character &ch,berserker &b ,thief &t,warrior &w,mage &m, bard &ba){
    cout<<"Choose a profession:"<<endl;
    cout<<"1.Berserker"<<endl;
    cout<<"2.Thief"<<endl;
    cout<<"3.Warrior"<<endl;
    cout<<"4.Mage"<<endl;
    cout<<"5.Bard"<<endl;
    int pr;
    cin>>pr;


    switch (pr){
        case 1:{
            b.profession(ch);
            break;
        }
        case 2:{
            t.profession(ch);
            break;
        }
        case 3:{
            w.profession(ch);
            break;
        }
        case 4:{
            m.profession(ch);
            break;
        }
        case 5:{
            ba.profession(ch );
        }
        default: {
            break;
        }
    }
}

void character::display() {
        cout<<endl;
        cout<<name<<" statistics: "<<endl;
        cout<<"Profession: "<<profession<<endl;
        cout<<"Level:"<<level<<endl;
        cout<<"Experience: "<<experience<<"/"<<maxeperiencelvl<<endl;
        cout<<"Health: "<<health<<"/"<<maxhealth<<endl;
        cout<<statname[0]<<": "<<statistics[0]<<endl;
        cout<<statname[1]<<": "<<statistics[1]<<endl;
        cout<<statname[2]<<": "<<statistics[2]<<endl;
        cout<<statname[3]<<": "<<statistics[3]<<endl;
        cout<<statname[4]<<": "<<statistics[4]<<endl;
        cout<<"Item: "<<charitem<<" + "<<charitembonus<<endl;
        cout<<endl;

}
