
#ifndef LAB12_BATTLE_H
#define LAB12_BATTLE_H

#include <stdlib.h>
#include "characters.h"
#include "monsters.h"
#include "templates.h"
#include "fightlist.h"
#include "items.h"



template <class T>
class battle {

public:


    void newl(string log) {
        fightel *p;
        p = new fightel;
        p->name = log;
        ft.push_head(p);
        if (ft.size() > 10) {
            ft.rmTail();
        }

    };

    int j;
    FightList ft;
    item i;

    virtual void battle_dispay(character &ch, monsters &m) {
        cout << "\t" << endl;
        cout << "\t" << ch.name << " statistics: " << "\t\t\t" << m.name << " statistics: " << endl;
        cout << "\t" << "Profession: " << ch.profession << "\t\t\t" << "Profession: " << m.profession << endl;
        cout << "\t" << "Level:" << ch.level << "\t\t\t\t\t" << "Level:" << m.level << endl;
        cout << "\t" << "Experience: " << ch.experience << "/" << ch.maxeperiencelvl << "\t\t\t" << "Experience: "
             << m.experience << "/" << m.maxeperiencelvl << endl;
        cout << "\t" << "Health: " << ch.health << "/" << ch.maxhealth << "\t\t\t\t" << "Health: " << m.health << "/"
             << m.maxhealth << endl;
        cout << "\t" << ch.statname[0] << ": " << ch.statistics[0] << "\t\t\t\t" << m.statname[0] << ": "
             << m.statistics[0] << endl;
        cout << "\t" << ch.statname[1] << ": " << ch.statistics[1] << "\t\t\t\t" << m.statname[1] << ": "
             << m.statistics[1] << endl;
        cout << "\t" << ch.statname[2] << ": " << ch.statistics[2] << "\t\t\t\t" << m.statname[2] << ": "
             << m.statistics[2] << endl;
        cout << "\t" << ch.statname[3] << ": " << ch.statistics[3] << "\t\t\t\t" << m.statname[3] << ": "
             << m.statistics[3] << endl;
        cout << "\t" << ch.statname[4] << ": " << ch.statistics[4] << "\t\t\t\t" << m.statname[4] << ": "
             << m.statistics[4] << endl;
        cout << "\tItem: " << ch.charitem << " + " << ch.charitembonus << endl;
        cout << "\t" << endl;
    };

    void startofthebattle() {

        //attributes a;
        cout << "Let's start the battle" << endl;
        character hero;
        hero.load();
        cout << endl;

        int stat0bon = 0;
        int stat1bon = 0;
        int stat2bon = 0;
        int stat3bon = 0;
        int stat4bon = 0;
        int healthbon = 0;

        if (hero.charitem == "Sword") {
            stat0bon = stat0bon + hero.charitembonus;
        } else if (hero.charitem == "Bow") {
            stat1bon = hero.charitembonus;
        } else if (hero.charitem == "Helmet") {
            stat2bon = hero.charitembonus;
        } else if (hero.charitem == "Wand") {
            stat3bon = hero.charitembonus;
        } else if (hero.charitem == "Lute") {
            stat4bon = hero.charitembonus;
        } else if (hero.charitem == "Potion") {
            healthbon = hero.charitembonus;
        } else if (hero.charitem == "none") {

        } else {}


        cout << "Which monster do you want to figth?" << endl;
        string nameofchar;
        cin >> nameofchar;
        for (j = 1; j < 6; j++) {
            int a;
            int am;
            monsters monster;
            monster.monsterloading(j, nameofchar);
            cout << endl;
            cout << "\t\t\tROUND " << j << endl;

            do {

                battle_dispay(hero, monster);

                cout << "\t\t\t Hero turn:" << endl;
                cout << endl;
                cout << "\t\t\tChoose your attack" << endl;
                cout << "\t\t\t1.Strength attack" << endl;
                cout << "\t\t\t2.Dexterity attack" << endl;
                cout << "\t\t\t3.Endurance attack" << endl;
                cout << "\t\t\t4.Intelligence attack" << endl;
                cout << "\t\t\t5.Charisma attack" << endl;
                cout << "\t\t\t6.Heal" << endl;
                cin >> a;
                cout << "\t\t\tHero choice: " << a << endl;


                switch (a) {
                    case 1: {
                        if (hero.statistics[0] + stat0bon >= monster.statistics[0]) {
                            monster.health = monster.health - 10;
                            monster.health = monster.health - stat0bon;

                            cout << "\t\t\tMonster attacked" << endl;

                        } else {
                            cout << "\t\t\tMonster dodged" << endl;
                            hero.health = hero.health - 5;
                        }
                        break;
                    }
                    case 2: {
                        if (hero.statistics[1] + stat1bon >= monster.statistics[1]) {
                            monster.health = monster.health - 10;
                            cout << "\t\t\tMonster attacked" << endl;
                            monster.health = monster.health - stat1bon;

                        } else {
                            cout << "\t\t\tMonster dodged" << endl;
                            hero.health = hero.health - 5;
                        }
                        break;
                    }
                    case 3: {
                        if (hero.statistics[2] + stat2bon >= monster.statistics[2]) {
                            monster.health = monster.health - 10;
                            cout << "\t\t\tMonster attacked" << endl;

                            monster.health = monster.health - stat2bon;

                        } else {
                            cout << "\t\t\tMonster dodged" << endl;
                            hero.health = hero.health - 5;
                        }
                        break;
                    }
                    case 4: {
                        if (hero.statistics[3] + stat3bon >= monster.statistics[3]) {
                            monster.health = monster.health - 10;

                            monster.health = monster.health - stat3bon;
                        } else {
                            cout << "\t\t\tMonster dodged" << endl;
                            hero.health = hero.health - 5;
                        }
                        break;
                    }
                    case 5: {
                        if (hero.statistics[4] + stat4bon >= monster.statistics[4]) {
                            monster.health = monster.health - 10;
                            cout << "\t\t\tMonster attacked" << endl;
                            monster.health = monster.health - stat4bon;

                        } else {
                            cout << "\t\t\tMonster dodged" << endl;
                            hero.health = hero.health - 5;
                        }
                        break;
                    }
                    case 6: {
                        if (hero.charitem == "Potion") {
                            hero.health = hero.health + healthbon;
                        } else {
                            cout << "\t\t\tYou do not have health potion!" << endl;
                        }
                        break;
                    }

                }


                battle_dispay(hero, monster);
                cout << "\t\t\t Monster turn:" << endl;
                cout << "\t\t\tChoose your attack" << endl;
                cout << "\t\t\t1.Strength attack" << endl;
                cout << "\t\t\t2.Dexterity attack" << endl;
                cout << "\t\t\t3.Endurance attack" << endl;
                cout << "\t\t\t4.Intelligence attack" << endl;
                cout << "\t\t\t5.Charisma attack" << endl;
                am = rand() % 5 + 1;
                cout << "\t\t\tMonster choice:" << am << endl;

                switch (am) {
                    case 1: {
                        if (hero.statistics[0] > monster.statistics[0]) {
                            monster.health = monster.health - 5;
                            cout << "\t\t\tHero dodged" << endl;
                        } else {
                            cout << "\t\t\tHero attacked" << endl;
                            hero.health = hero.health - 10;
                        }
                        break;
                    }
                    case 2: {
                        if (hero.statistics[1] > monster.statistics[1]) {
                            monster.health = monster.health - 5;
                            cout << "\t\t\tHero dodged" << endl;
                        } else {
                            cout << "\t\t\tHero attacked" << endl;
                            hero.health = hero.health - 10;
                        }
                        break;
                    }
                    case 3: {
                        if (hero.statistics[2] > monster.statistics[2]) {
                            monster.health = monster.health - 5;
                            cout << "\t\t\tHero dodged" << endl;
                        } else {
                            cout << "\t\t\tHero attacked" << endl;
                            hero.health = hero.health - 10;
                        }
                        break;

                    }
                    case 4: {
                        if (hero.statistics[3] > monster.statistics[3]) {
                            monster.health = monster.health - 5;
                            cout << "\t\t\tHero dodged" << endl;
                        } else {
                            cout << "\t\t\tHero attacked" << endl;
                            hero.health = hero.health - 10;
                        }
                        break;
                    }
                    case 5: {
                        if (hero.statistics[4] > monster.statistics[4]) {
                            monster.health = monster.health - 5;
                            cout << "\t\t\tHero dodged" << endl;
                        } else {
                            cout << "\t\t\tHero attacked" << endl;
                            hero.health = hero.health - 10;
                        }
                        break;
                    }

                }


            } while (hero.health > 0 && monster.health > 0);
            if (monster.health <= 0) {
                cout << endl;
                cout << "\t\t\tYou won" << endl;
                i.itempool(j);

                string listshow = "Victory";
                newl(listshow);


                int newex;
                newex = hero.experience + monster.experience;
                if (newex >= hero.maxeperiencelvl) {
                    hero.experience = newex % hero.maxeperiencelvl;
                    hero.level = hero.level + 1;
                    hero.maxeperiencelvl = hero.maxeperiencelvl * 1.2 * hero.level;

                } else {
                    hero.experience = newex;
                }
                hero.health = 100;
                hero.save();


                if (j < 5) {

                    cout << "1.Go to the next round" << endl;
                    cout << "2.Load battle history" << endl;
                    cout << "3.Character inventory" <<endl;
                    cout << "4.Return to main menu" << endl;

                    int ch;
                    cin >> ch;
                    if (ch == 1) {
                        cout << "Loading next monster..." << endl;
                        cout << endl;
                    } else if (ch == 2) {
                        cout << endl;
                        cout << "Battle history: " << endl;
                        ft.showNames();
                        cout << endl;
                        cout << "1.Go to the next round" << endl;
                        cout << "2.Return to main menu" << endl;
                        int ch2;
                        cin >> ch2;
                        if (ch2 == 1) {
                            cout << "Loading next monster..." << endl;
                            cout << endl;
                        } else if (ch2 == 2) {
                            cout << "See you next time" << endl;
                            cout << endl;
                            j = 7;
                        }
                    }else if (ch == 3) {
                        item i;
                        i.inventory();
                    }
                    else if (ch == 4) {
                        cout << "See you next time" << endl;
                        cout << endl;
                        j = 7;
                    }

                }
            }


                //end
            else if (hero.health <= 0) {
                cout << endl;
                cout << "\t\t\tYou lost " << endl;
                string listshow = "Defeat";
                newl(listshow);
                //cin >> listshow;

                int losex;
                losex = hero.experience - monster.experience;
                if (losex < 0) {
                    hero.maxeperiencelvl = hero.maxeperiencelvl / 1.2 / hero.level;
                    hero.level = hero.level - 1;
                    hero.experience = hero.maxeperiencelvl + losex;

                } else {
                    hero.experience = losex;
                }
                hero.health = 100;
                hero.save();
                cout << "1.Load battle history" << endl;
                cout << "2.Return to main menu" << endl;

                int ch;
                cin >> ch;
                if (ch == 1) {
                    cout << endl;
                    cout << "Battle history: " << endl;
                    ft.showNames();
                    cout << endl;
                    j = 7;
                }
                else if (ch == 2) {
                    cout << "See you next time" << endl;
                    cout << endl;
                    j = 7;

                    //


                }
            }


        };
    };
};




#endif //LAB12_BATTLE_H
