
#include <iostream>
#include <string>
#include <fstream>
#include<sstream>
#include<bits/stdc++.h>
#include <filesystem>

#include "characters.h"
#include "monsters.h"


monsters::monsters (string n,string p,int l, int ex,int mex,int h, int s, int d,int e,int i ,int c) {
    name = n;
    profession=p;
    level=l;
    experience=ex;
    maxeperiencelvl=mex;
    health=h;
    statistics[0] = s;
    statistics[1] = d;
    statistics[2] = e;
    statistics[3] = i;
    statistics[4] = c;
}


monsters::~monsters(){
}

void monsters::attgenerator(int j){

    string monstertype[11]={"Dragon Lord","Tarantula","Giant Spider","Orc Berserker","Juggernaut","Hellhound","Dwarf Guard","Demodras","Morgaroth","Ghazbaran","Phantasm"};
    int no= rand() % 10;
    name=monstertype[no];
    profession= "monster";
    level=0+j;
    maxeperiencelvl=level*10;
    experience= rand() % 11 + (level-1)*10;
    health=100;
    statistics[0] = rand() % 4 + (level+2);
    statistics[1] = rand() % 4 + (level+2);
    statistics[2] = rand() % 4 + (level+2);
    statistics[3] = rand() % 4 + (level+2);
    statistics[4] = rand() % 4 + (level+2);
}
void monsters::monstersaving(int j,string namemon){
    string nameofchar;
    switch (j){
        case 1:
        {
            string filename = namemon+ "1.txt";
            ofstream e(filename.c_str());

            e<<name<<endl;
            e<<profession<<endl;
            e<<level<<endl;
            e<<experience<<endl;
            e<<maxeperiencelvl<<endl;
            e<<health<<endl;
            e<<statistics[0]<<endl;
            e<<statistics[1]<<endl;
            e<<statistics[2]<<endl;
            e<<statistics[3]<<endl;
            e<<statistics[4]<<endl;

            e.close();
        }
        case 2:
        {
            string filename = namemon+ "2.txt";
            ofstream e(filename.c_str());

            e<<name<<endl;
            e<<profession<<endl;
            e<<level<<endl;
            e<<experience<<endl;
            e<<maxeperiencelvl<<endl;
            e<<health<<endl;
            e<<statistics[0]<<endl;
            e<<statistics[1]<<endl;
            e<<statistics[2]<<endl;
            e<<statistics[3]<<endl;
            e<<statistics[4]<<endl;

            e.close();
        }
        case 3:
        {
            string filename = namemon+ "3.txt";
            ofstream e(filename.c_str());

            e<<name<<endl;
            e<<profession<<endl;
            e<<level<<endl;
            e<<experience<<endl;
            e<<maxeperiencelvl<<endl;
            e<<health<<endl;
            e<<statistics[0]<<endl;
            e<<statistics[1]<<endl;
            e<<statistics[2]<<endl;
            e<<statistics[3]<<endl;
            e<<statistics[4]<<endl;

            e.close();
        }
        case 4:
        {
            string filename = namemon+ "4.txt";
            ofstream e(filename.c_str());

            e<<name<<endl;
            e<<profession<<endl;
            e<<level<<endl;
            e<<experience<<endl;
            e<<maxeperiencelvl<<endl;
            e<<health<<endl;
            e<<statistics[0]<<endl;
            e<<statistics[1]<<endl;
            e<<statistics[2]<<endl;
            e<<statistics[3]<<endl;
            e<<statistics[4]<<endl;

            e.close();
        }
        case 5:
        {
            string filename = namemon+ "5.txt";
            ofstream e(filename.c_str());

            e<<name<<endl;
            e<<profession<<endl;
            e<<level<<endl;
            e<<experience<<endl;
            e<<maxeperiencelvl<<endl;
            e<<health<<endl;
            e<<statistics[0]<<endl;
            e<<statistics[1]<<endl;
            e<<statistics[2]<<endl;
            e<<statistics[3]<<endl;
            e<<statistics[4]<<endl;

            e.close();
        }
    }


}

void monsters::monsterloading(int j,string nameofchar) {


    cout << endl;
    switch (j) {
        case 1: {
            string filename = nameofchar + "1.txt";
            fstream file;
            nameofchar.clear();
            file.open(filename.c_str(), ios::in | ios::out);
            if (file.good() == true) {

                string exch;
                file >> name;
                file >> profession;

                file >> exch;
                level = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                experience = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                maxeperiencelvl = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                health = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                statistics[0] = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                statistics[1] = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                statistics[2] = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                statistics[3] = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                statistics[4] = atoi(exch.c_str());
                exch.clear();

                display();
            }
            file.close();
        }
        case 2: {
            string filename = nameofchar + "2.txt";
            fstream file;
            nameofchar.clear();
            file.open(filename.c_str(), ios::in | ios::out);
            if (file.good() == true) {

                string exch;
                file >> name;
                file >> profession;

                file >> exch;
                level = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                experience = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                maxeperiencelvl = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                health = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                statistics[0] = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                statistics[1] = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                statistics[2] = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                statistics[3] = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                statistics[4] = atoi(exch.c_str());
                exch.clear();

                display();
            }
            file.close();
        }
        case 3: {
            string filename = nameofchar + "3.txt";
            fstream file;
            nameofchar.clear();
            file.open(filename.c_str(), ios::in | ios::out);
            if (file.good() == true) {

                string exch;
                file >> name;
                file >> profession;

                file >> exch;
                level = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                experience = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                maxeperiencelvl = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                health = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                statistics[0] = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                statistics[1] = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                statistics[2] = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                statistics[3] = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                statistics[4] = atoi(exch.c_str());
                exch.clear();

                display();
            }
            file.close();
        }
        case 4: {
            string filename = nameofchar + "4.txt";
            fstream file;
            nameofchar.clear();
            file.open(filename.c_str(), ios::in | ios::out);
            if (file.good() == true) {

                string exch;
                file >> name;
                file >> profession;

                file >> exch;
                level = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                experience = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                maxeperiencelvl = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                health = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                statistics[0] = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                statistics[1] = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                statistics[2] = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                statistics[3] = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                statistics[4] = atoi(exch.c_str());
                exch.clear();

                display();
            }
            file.close();
        }
        case 5: {
            string filename = nameofchar + "5.txt";
            fstream file;
            nameofchar.clear();
            file.open(filename.c_str(), ios::in | ios::out);
            if (file.good() == true) {

                string exch;
                file >> name;
                file >> profession;

                file >> exch;
                level = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                experience = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                maxeperiencelvl = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                health = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                statistics[0] = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                statistics[1] = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                statistics[2] = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                statistics[3] = atoi(exch.c_str());
                exch.clear();

                file >> exch;
                statistics[4] = atoi(exch.c_str());
                exch.clear();

                display();
            }
            file.close();
        }
    }
}

void monsters::monstercreation(int j,string namemon){
    attgenerator(j);
    display();
    monstersaving(j,namemon);

}

void monsters::display() {
    cout<<endl;
    cout<<name<<" statistics: "<<endl;
    cout<<"Profession: "<<profession<<endl;
    cout<<"Level:"<<level<<endl;
    cout<<"Experience: "<<experience<<"/"<<maxeperiencelvl<<endl;
    cout<<"Health: "<<health<<"/"<<maxhealth<<endl;
    cout<<statname[0]<<": "<<statistics[0]<<endl;
    cout<<statname[1]<<": "<<statistics[1]<<endl;
    cout<<statname[2]<<": "<<statistics[2]<<endl;
    cout<<statname[3]<<": "<<statistics[3]<<endl;
    cout<<statname[4]<<": "<<statistics[4]<<endl;
    cout<<endl;

}
