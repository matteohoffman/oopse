
#include <iostream>
#include <string>
#include <fstream>
#include<sstream>
#include<bits/stdc++.h>
#include<cstring>

#include "characters.h"
#include "monsters.h"
#include "battle.h"

using namespace std;
bool run=true;

void operation(){
    fightel *el;
    character character;
    monsters m1;
    monsters m2;
    monsters m3;
    monsters m4;
    monsters m5;
    while (run==true){
        int decision;
        cout<<"SMALL RPG GAME"<<endl;
        cout<<"1.Create character"<<endl;
        cout<<"2.Load character"<<endl;
        cout<<"3.Monster Generator"<<endl;
        cout<<"4.Load monsters"<<endl;
        cout<<"5.Fight"<<endl;
        cout<<"6.Inventory"<<endl;
        cout<<"7.Search item"<<endl;
        cout<<"8.Quit"<<endl;
        cin>>decision;

        switch (decision){
            case 1:{
                character.add();
                break;
            }
            case 2:{
                character.load();
                break;
            }
            case 3:{
                cout<<"Name your monster list"<<endl;
                string namemon;
                cin>>namemon;

                m1.monstercreation(1,namemon);
                m2.monstercreation(2,namemon);
                m3.monstercreation(3,namemon);
                m4.monstercreation(4,namemon);
                m5.monstercreation(5,namemon);

                break;
            }
            case 4:{
                cout << endl;
                cout << "Which monster do you want to load?" << endl;
                string nameofchar;
                cin >> nameofchar;
                cout << "Loading..." << endl;
                m1.monsterloading(1,nameofchar);
                m2.monsterloading(2,nameofchar);
                m3.monsterloading(3,nameofchar);
                m4.monsterloading(4,nameofchar);
                m5.monsterloading(5,nameofchar);
                break;
            }
            case 5:{
                battle<int> startbattle;
                startbattle.startofthebattle();
                break;
            }
            case 6:{
                item i;
                i.inventory();
                break;
            }
            case 7:{
                item i;
                int returnn;
                cout<<"Enter the name of the item you are looking for: "<<endl;
                string find;
                cin>>find;
                ifstream f ("inventory1.txt");
                string l;
                int c;
                while (getline(f,l)){
                    c++;
                }
                i.itemfind(1,find,c);
                break;
            }
            case 8:{
                run=false;
                cout<<"See you next time"<<endl;
                break;
            }

            default :{
                cout<<"Invalid value!"<<endl;
                break;
            }
        }

    }
}

