
#ifndef LAB15_TEMPLATES_H
#define LAB15_TEMPLATES_H


#include <iostream>
#include <string>

using namespace std;

class attributes{
public:
    string name;
    string statname[5]={"Strength", "Dexterity","Endurance","Intelligence","Charisma"};
    int statistics[5];
    string profession;
    int experience;
    int level;
    int maxeperiencelvl;
    int health;
    int maxhealth=100;
    string charitem;
    int charitembonus;




    virtual void display()=0;


};

class proffesiontemp {
public:
    virtual void profession()=0;
};

#endif //LAB15_TEMPLATES_H
