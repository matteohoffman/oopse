
#ifndef LAB15_CHARACTERS_H
#define LAB15_CHARACTERS_H

#include "templates.h"
using namespace std;

class berserker;
class thief;
class warrior;
class mage;
class bard;

class character: public attributes {
public:

    character(string="none",string="none",int=0,int=0,int=0,int=0, int=0, int=0, int=0, int=0, int=0,string="none" ,int=0);
    virtual ~character();

    virtual void add();
    virtual void save();
    virtual void load();
    virtual void display();


    friend class berserker;
    friend class thief;
    friend class warrior;
    friend class mage;
    friend class bard;


    void choose(character &ch, berserker &b ,thief &t,warrior &w,mage &m,bard &ba);
};

class berserker {
    //strength
public :
    void profession(character &ch){
        ch.profession="berserker";
        ch.statistics[0]=ch.statistics[0]+5;

    }

};
class thief{
    //dexterity
public :
    void profession(character &ch){
        ch.profession="thief";
        ch.statistics[1]=ch.statistics[1]+5;

    }
};
class warrior{
    //endurance
public :
    void profession(character &ch){
        ch.profession="warrior";
        ch.statistics[2]=ch.statistics[2]+5;

    }
};
class mage{
    //intelligence
public :
    void profession(character &ch){
        ch.profession="mage";
        ch.statistics[3]=ch.statistics[3]+5;

    }
};

class bard {
    //charisma
public :
    void profession(character &ch) {
        ch.profession = "bard";
        ch.statistics[4] = ch.statistics[4] + 5;
    }

    };


#endif //LAB15_CHARACTERS_H
