
#ifndef LAB15_MONSTERS_H
#define LAB15_MONSTERS_H

#include "templates.h"
using namespace std;

class monsters: public character{
public:

    monsters(string="none",string="none",int=0,int=0,int=0,int=0, int=0, int=0, int=0, int=0, int=0);
    virtual ~monsters();

    virtual void attgenerator(int j);
    virtual void monstersaving(int j,string namemon);
    virtual void monsterloading(int j,string nameofchar);
    virtual void monstercreation(int j,string namemon);
    virtual void display();

};
#endif //LAB15_MONSTERS_H
