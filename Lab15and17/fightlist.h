
#ifndef LAB14_FIGHTLIST_H
#define LAB14_FIGHTLIST_H

//Element declaration
struct fightel{
    fightel * next;
    string name;

};


//definition of singly listed list class
class FightList {
    fightel *head, *tail;
    int cnt;

public:
    FightList() {
        head = tail = NULL;
        cnt = 0;
    }

    ~FightList() {
        fightel *el;

        while (head) {
            el = head->next;
            delete head;
            head = el;
        }
    }

    //Method returning list size
    unsigned size() {
        return cnt;
    }

    //Method adding an element at the front of the list
    fightel *push_head(fightel *el) {
        el->next = head;
        head = el;
        if (!tail) tail = head;
        cnt++;
        return head;
    }


    //Method deleting the last element of the list
    fightel *rmTail() {
        fightel *el;

        if (tail) {
            el = tail;
            if (el == head) head = tail = NULL;
            else {
                tail = head;
                while (tail->next != el) tail = tail->next;
                tail->next = NULL;
            }
            cnt--;
            return el;
        } else return NULL;
    }





    //Method used to display data stored in the list

    void  showNames() {
        fightel *el;

        if (!head) cout << "List is empty." << endl;
        else {
            el = head;
            while (el) {
                cout << el->name << " " << endl;
                el = el->next;
            }
            cout << endl;
        }
    }

};

#endif //LAB14_FIGHTLIST_H
